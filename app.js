// eslint.detect-eval-with-expression
function dangerous_eval(myeval) {
  eval(myeval);
}

// eslint.detect-object-injection
function handler(anyVal, userInput) {
  user[anyVal] = user[userInput[0]](userInput[1]);
}

